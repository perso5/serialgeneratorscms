def variateur(str, index, type):

    dict_variation = {'alpha': 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'num': '0123456789'}
    resultat = []
    type = type.split(' ')

    for t in type:
        variation_list = dict_variation[t]

        for i in variation_list:
            a = str[0:index] + i + str[index + 1:]
            resultat.append(a)

    return resultat


def trieur_par_model(marque):

    # On lit le fichier avec les clés valides
    lignes = ''

    with open(marque + '_1_valides.csv', 'r') as f:
        lignes = f.read().splitlines()

    lignes = [eval(li) for li in lignes if li != '']

    # Pour chaque ligne du fichier, on ne garde que les champs qui nous intéressent
    r = list()
    for d in lignes:
        r.append([d['serialNumber'], d['productNumber'], d['seriesName'], d['status'], str(d['seriesOid']), str(d['modelOid']),
                  d['warrantyResultList'][0]['obligationIdentifier'], d['warrantyResultList'][0]['warrantyType'],
                  d['warrantyResultList'][0]['obligationStartDate'], d['warrantyResultList'][0]['obligationEndDate']])
    lignes = r

    # On prepare l'exportation pour des fichiers séparés
    dict_export = {}
    for li in lignes:
        if li[2] not in dict_export.keys():
            dict_export[li[2]] = []
        dict_export[li[2]].append(li)

    # On réécrit les résultats dans le bon fichier
    for k, v in dict_export.items():

        #v = [str(a) for a in v]

        with open(marque + '_2_' + k + '.csv', 'a') as f:
            resultat = [';'.join(ligne) for ligne in v]
            resultat = '\n'.join(resultat) + '\n'
            f.write(resultat)

    with open(marque + '_1_valides.csv', 'w') as f:
        f.write('')

    print('fin du tri')

def from_to(sn, moving):

    dict_variation = {'alpha': 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'num': '0123456789'}
    variation_list = ''
    resultat = [sn]

    for i in moving:

        if sn[i] in dict_variation['alpha']:
            variation_list = dict_variation['alpha']
        elif sn[i] in dict_variation['num']:
            variation_list = dict_variation['num']

        resultat2 = []
        for sn in resultat:
            for c in variation_list:
                resultat2.append(sn[:i] + c + sn[i + 1:])
        resultat = list(resultat2)

    print('Lignes genérées (' + str(len(resultat)) + ').')
    return resultat


def ecrire_cles_brutes(marque, cles):
    with open(marque + '_1_cles_brutes.txt', 'w') as f:
        cles = '\n'.join(cles)
        f.write(cles)
        print(str(len(cles.split('\n'))) + " lignes ecrites dans '" + marque + "_1_cles_brutes.txt'.")


def lire_parametres(marque):
    with open(marque + '_0_parametres.txt', 'r') as f:
        param = eval(f.read())

    print('Parametres ' + marque + 'lus avec succes.')
    return param.values()
