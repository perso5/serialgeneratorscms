######################################################################
#                             VERSION 1.0                            #
#                                                                    #
#              Cette version est : crade, pas fiabilisé,             #
#                   pas optimisée et mal commentée.                  #
#                                                                    #
#                          Made in chez moi                          #
######################################################################

from selenium import webdriver, common
import json
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
import time


def HP_checker(seeked_run, seeked_pn=''):

    if seeked_run not in ['first_run', 'second_run'] or (seeked_run == 'second_run' and seeked_pn == ''):
        raise Exception("gros con t'as oublié les arguments")

    # ---------- 1 Ouvrir la page

    options = Options()
    # options.headless = True
    options.executable_path = '_geckodriver'
    url = 'https://support.hp.com/us-en/checkwarranty/multipleproducts'
    driver = webdriver.Firefox(options=options)

    # ----- 2.0 La boucle

    valides = list()
    invalides = list()
    need_pn = list()

    number_one = 'CN484206AA'
    tick = 0.4
    start = time.time()

    # ---------- 0 On charge en mémoire un fichier de serials

    cles_brut = ''

    if seeked_run == 'first_run':

        with open('HP_1_cles_brutes.txt', 'r') as f:
            cles_brut = f.read().splitlines()
        cles_brut = [cle for cle in cles_brut if cle != '']

    elif seeked_run == 'second_run':

        with open('HP_1_need_pn.csv', 'r') as f:
            cles_brut = f.read().splitlines()
        cles_brut = [eval(cle)["serialNumber"] for cle in cles_brut if cle != '']

    cle_brutes_groupees = [[number_one] + cles_brut[i:i + 19] for i in range(0, len(cles_brut), 19)]

    for i, groupe in enumerate(list(cle_brutes_groupees)):

        # ----- 2.1 Remplir la page et lancer

        driver.get(url)

        def remplir_infos():
            for y, cle_a_tester in enumerate(list(groupe)):

                ok = False
                while ok is not True:
                    try:
                        serial_field = driver.find_element_by_css_selector('#wFormSerialNumber' + str(y + 1))
                        serial_field.send_keys(cle_a_tester)
                        ok = True
                    except:
                        print('[' + str(i * y) + '/' + str(len(cles_brut)) + ']', [cle_a_tester], 'Page 1 still loading')
                        time.sleep(tick)

            global submit
            submit = driver.find_element_by_css_selector('#btnWFormSubmit')
            submit.send_keys(Keys.ENTER)

        remplir_infos()

        if driver.current_url == 'https://support.hp.com/us-en/error?errorType=500':
            driver.get(url)
            remplir_infos()

        # 3 cas :
        # - Ceux qui sont bons dès le début => Leur attribut data-ajaxresp possède toutes les infos (status="IW")
        # - Ceux qui demandent un PN => l'attribut data-ajaxresp eciste mais ne possède pas les infos (status="MI")
        # - Ceux qui sont mauvais =>  N'a pas l'attribut data-ajaxresp

        # attendre fin chargement
        print(driver.execute_script('return document.readyState;'))
        while driver.execute_script('return document.readyState;') != 'complete':
            print('Page 1 still loading after first submit')
            time.sleep(tick)

        if seeked_run == 'second_run':

            driver.find_element_by_css_selector('#wFormRemoveButton1').send_keys(Keys.ENTER)
            serial_field = driver.find_element_by_css_selector('#wFormSerialNumber1')
            serial_field.send_keys(number_one)

            for y, cle_a_tester in enumerate(list(groupe)[1:]):

                productNum_field = driver.find_element_by_css_selector('#wFormProductNum' + str(y + 2))
                productNum_field.send_keys(seeked_pn)

            submit = driver.find_element_by_css_selector('#btnWFormSubmit')
            submit.send_keys(Keys.ENTER)

            # attendre fin chargement
            est_ce_que_charge = [None]
            while None in est_ce_que_charge:
                est_ce_que_charge = [driver.find_element_by_css_selector('#wFormSerialNumber' + str(i)).get_attribute('data-ajaxresp') for i in range(2, len(groupe) + 1)]
                print('Page 1 still loading after first submit')
                time.sleep(tick)

        # Pour chaque ligne
        for i in range(2, 21):

            # On recupère la valeur de l'attribut data-ajaxresp
            data_ajaxresp = driver.find_element_by_css_selector('#wFormSerialNumber' + str(i)).get_attribute('data-ajaxresp')
            if data_ajaxresp is None:
                continue
            data_ajaxresp = json.loads(data_ajaxresp)

            # On fait le tri dans les status :
            # IW = good, MI = need PN, UN = weirdos, ...
            status = data_ajaxresp["status"]

            if status == 'IW':
                valides.append(data_ajaxresp)
                print(data_ajaxresp["serialNumber"], 'is valid. (ligne' + str(i) + ')')
            elif status == 'MI':
                need_pn.append(data_ajaxresp)
                print(data_ajaxresp["serialNumber"], 'need product number. (ligne' + str(i) + ')')
            else:
                invalides.append(data_ajaxresp)
                print(data_ajaxresp["serialNumber"], 'is invalid. (ligne' + str(i) + ')')

            # Puis on nettoie pour préparer le prochain groupe
            not_ok = True
            while not_ok:
                try:
                    driver.find_element_by_css_selector('#wFormRemoveButton' + str(i)).send_keys(Keys.ENTER)
                    not_ok = False
                except common.exceptions.ElementNotInteractableException:
                    print('css still loading')

        input()

        if seeked_run == 'first_run':

            with open('HP_1_cles_brutes.txt', 'r') as f:
                cles = f.read().splitlines()
            cles = [cle for cle in cles if cle not in [''] + groupe]
            with open('HP_1_cles_brutes.txt', 'w') as f:
                cles = '\n'.join(cles)
                f.write(cles)

            with open('HP_1_need_pn.csv', 'a') as f:
                need_pn = [str(n) for n in need_pn]
                need_pn = '\n'.join(need_pn) + '\n'
                f.write(need_pn)
                need_pn = []

        elif seeked_run == 'second_run':

            with open('HP_1_need_pn.csv', 'r') as f:
                cles = f.read().splitlines()
            cles = [cle for cle in cles if cle not in [''] + groupe]
            with open('HP_1_need_pn.csv', 'w') as f:
                cles = '\n'.join(cles) + '\n'
                f.write(cles)

        with open('HP_1_valides.csv', 'a') as f:
            valides = [str(v) for v in valides]
            valides = '\n'.join(valides) + '\n'
            f.write(valides)
            valides = []

        with open('HP_1_invalides.csv', 'a') as f:
            invalides = [str(inv) for inv in invalides]
            invalides = [inv for inv in invalides if inv not in [''] + groupe]
            invalides = '\n'.join(invalides) + '\n'
            f.write(invalides)
            invalides = []

    # ---------- 3 On sauvegarde les résultats

    driver.close()

    end = time.time()

    print(str(end - start) + 's', 'pour', len(cles_brut), 'cles. Soit ' + str((end - start) / len(cles_brut)) + 's par cle.')


import _checker_common as ccommon

sn, seeked_pn, moving = ccommon.lire_parametres('HP')

resultats = ccommon.from_to(sn, moving)
ccommon.ecrire_cles_brutes('HP', resultats)

# HP_checker('first_run')
HP_checker('second_run', seeked_pn)

ccommon.trieur_par_model('HP')
